//
//  Item.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 20/02/21.
//

import Foundation

class Item: NSObject {
    var text: String = ""
    var value: Int = 0
    
    init(text: String, value: Int) {
        self.text = text
        self.value = value
    }
}
