//
//  ProfileModels.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 18/02/21.
//

import SwiftyJSON

public class ProfileModels: NSObject {
    var id: String = ""
    var name: String = ""
    var level: Int = 0
    var age: Int = 0
    var birthday: String = ""
    var gender: String = ""
    var zodiac: String = ""
    var hometown: String = ""
    var bio: String = ""
    var latlong: String = ""
    var education: EducationModel?
    var career: CareerModel?
    var userPictures: [UserPictureModel?] = []
    var userPicture: UserPictureModel?
    var coverPicture: String = ""
    
    init(_ json: JSON) {
        id = json["id"].stringValue
        name = json["name"].stringValue
        level = json["level"].intValue
        age = json["age"].intValue
        birthday = json["birthday"].stringValue
        hometown = json["hometown"].stringValue
        gender = json["gender"].stringValue
        zodiac = json["zodiac"].stringValue
        bio = json["bio"].stringValue
        latlong = json["latlong"].stringValue
        education = EducationModel(json["education"])
        career = CareerModel(json["career"])
        userPicture = UserPictureModel(json["user_picture"])
        coverPicture = json["cover_picture"]["url"].stringValue
        
        for detail in json["user_pictures"].arrayValue {
            userPictures.append(UserPictureModel(detail))
        }
    }
}

