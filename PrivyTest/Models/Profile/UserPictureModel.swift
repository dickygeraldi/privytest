//
//  UserPictureModel.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 18/02/21.
//

import SwiftyJSON

public class UserPictureModel: NSObject {
    var id: String = ""
    var url: String = ""
    
    init(_ json: JSON) {
        id = json["id"].stringValue
        url = json["picture"]["url"].stringValue
    }
}
