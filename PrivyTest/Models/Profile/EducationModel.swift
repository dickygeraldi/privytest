//
//  EducationModel.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 18/02/21.
//

import SwiftyJSON

public class EducationModel: NSObject {
    var schollName: String = ""
    var graduationTime: String = ""
    
    init(_ json: JSON) {
        schollName = json["school_name"].stringValue
        graduationTime = json["graduation_time"].stringValue
    }
}
