//
//  CareerModel.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 18/02/21.
//

import SwiftyJSON

public class CareerModel: NSObject {
    var companyName: String = ""
    var startFrom: String = ""
    var endIn: String = ""
    
    init(_ json: JSON) {
        companyName = json["company_name"].stringValue
        startFrom = json["starting_from"].stringValue
        endIn = json["ending_in"].stringValue
    }
}
