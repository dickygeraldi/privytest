//
//  UserDevice.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 16/02/21.
//

import SwiftyJSON

public class UserDeviceModel: NSObject {
    var deviceToken: String = ""
    var deviceType: String = ""
    var deviceStatus: String = ""
    
    init(_ json: JSON) {
        deviceToken = json["device_token"].stringValue
        deviceType = json["device_type"].stringValue
        deviceStatus = json["device_status"].stringValue
    }
}
