//
//  AuthModel.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 16/02/21.
//

import SwiftyJSON

public class AuthModel: NSObject {
    var id: String = ""
    var phone: String = ""
    var userStatus: String = ""
    var userType: String = ""
    var sugarId: String = ""
    var country: String = ""
    var latlong: String = ""
    var userDevice: UserDeviceModel?
    
    init(_ json: JSON) {
        id = json["id"].stringValue
        phone = json["phone"].stringValue
        userStatus = json["user_status"].stringValue
        userType = json["user_type"].stringValue
        sugarId = json["sugar_id"].stringValue
        country = json["country"].stringValue
        latlong = json["latlong"].stringValue
        userDevice = UserDeviceModel(json["user_device"])
    }
}
