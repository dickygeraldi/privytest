//
//  UserToken.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 18/02/21.
//

import SwiftyJSON

public class UserToken: NSObject {
    var accessToken: String = ""
    var tokenType: String = ""
    
    init(_ json: JSON) {
        accessToken = json["access_token"].stringValue
        tokenType = json["token_type"].stringValue
    }
}
