//
//  NetworkResponse.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 15/02/21.
//

import SwiftyJSON

public class NetworkResponse {
    public static let RETRYABLE_ERROR = -1000
    
    public var request: URLRequest?
    public var response: Any?
    public var reqCode: Int = 0
    public var statusCode: Int?
    public var errorMessage: String?

    public init(requestCode reqCode: Int = 0) {
        self.reqCode = reqCode
    }

    public init(requestCode reqCode: Int = 0, withStatusCode statusCode: Int) {
        self.reqCode = reqCode
        self.statusCode = statusCode
        
        if isServerError() && !isMaintenanceError() && !isTooManyRequestError() {
            self.errorMessage = "Gagal Menghubungi Server"
        }
    }
    
    public init(requestCode reqCode: Int = 0, withResponse response: Any) {
        self.response = response
        self.reqCode = reqCode
    }
    
    public func isSuccess() -> Bool {
        return errorMessage?.isEmpty ?? true && response != nil && !isServerError() && !isLocalDeviceError()
    }
    
    public func isServerError() -> Bool {
        return statusCode != nil && ((statusCode! >= 500 && statusCode! <= 503) || (statusCode! >= 400 && statusCode! <= 419) || isTooManyRequestError())
    }
    
    public func isLocalDeviceError() -> Bool {
        return isDeviceTokenExpired() || isUserTokenExpired() || isTimeMissMatch()
    }
    
    public func isHostNotFound() -> Bool {
        return statusCode == URLError.Code.cannotFindHost.rawValue
    }
    
    public func isTimeoutError() -> Bool {
        return statusCode == URLError.Code.timedOut.rawValue
    }
    
    public func isMaintenanceError() -> Bool {
        return statusCode == 503
    }
    
    public func isTooManyRequestError() -> Bool {
        return statusCode == 429
    }
    
    public func isRetryableError() -> Bool {
        return statusCode == NetworkResponse.RETRYABLE_ERROR
    }
    
    public func isDeviceTokenExpired() -> Bool {
        return isErrorOnDevice(with: 330)
    }
    
    public func isUserTokenExpired() -> Bool {
        return isErrorOnDevice(with: 333)
    }
    
    public func isTimeMissMatch() -> Bool {
        return isErrorOnDevice(with: 444)
    }
    
    public func isErrorOnDevice(with errorCode: Int) -> Bool {
        guard let response = self.response else {
            return false
        }
        
        let json = JSON(response)
        
        guard !json["status"].boolValue else {
            return false
        }
        
        return json["meta"]["code"].intValue == errorCode
    }
}
