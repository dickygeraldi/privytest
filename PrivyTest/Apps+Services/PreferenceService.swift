//
//  PreferenceService.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 15/02/21.
//

import Foundation

class PreferenceService {
    
    static let LOGIN_STATUS = "login_user"
    static let AUTH_KEY = "auth_user"
    static let DEVICE_KEY = "device_user"
    
    static func saveLoginStatus(_ loginStatus: Bool) {
        let preferences = UserDefaults.standard
        
        preferences.set(loginStatus, forKey: LOGIN_STATUS)
        
        preferences.synchronize()
    }
    
    static func isLogin() -> Bool {
        let preferences = UserDefaults.standard
        
        if preferences.object(forKey: LOGIN_STATUS) == nil {
            return false
        }
        return preferences.bool(forKey: LOGIN_STATUS)
    }
    
    static func saveAuthToken(_ authKey: String) {
        let preferences = UserDefaults.standard
        
        preferences.set(authKey, forKey: AUTH_KEY)
        
        preferences.synchronize()
    }
    
    static func getAuthtoken() -> String {
        let preferences = UserDefaults.standard
        
        if preferences.object(forKey: AUTH_KEY) == nil {
            return ""
        }
        return preferences.string(forKey: AUTH_KEY) ?? ""
    }
    
    static func saveDeviceKey(_ deviceKey: String) {
        let preferences = UserDefaults.standard
        
        preferences.set(deviceKey, forKey: DEVICE_KEY)
        
        preferences.synchronize()
    }
    
    static func getDeviceKey() -> String {
        let preferences = UserDefaults.standard
        
        if preferences.object(forKey: DEVICE_KEY) == nil {
            return ""
        }
        return preferences.string(forKey: DEVICE_KEY) ?? ""
    }
}
