//
//  ListUrls.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 15/02/21.
//

struct ListUrls {
    public static let REGISTRATION = "/register"
    public static let LOGIN = "/oauth/sign_in"
    public static let OTP_REQ = "/register/otp/request"
    public static let VALIDATE_OTP = "/register/otp/match"
    public static let GET_PROFILE = "/profile/me"
    public static let UPDATE_PROFILE = "/profile"
    public static let UPDATE_PROFILE_EDU = "/profile/education"
    public static let UPDATE_PROFILE_CAREER = "/profile/career"
    public static let UPDATE_COVER_IMAGE = "/uploads/cover"
    public static let UPDATE_PROFILE_PHOTO = "/uploads/profile"
}

