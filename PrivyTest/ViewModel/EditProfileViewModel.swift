//
//  EditProfileViewModel.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 19/02/21.
//

import RxSwift
import RxCocoa
import SwiftyJSON

class EditProfileViewModel: NetworkViewModel {
    let disposeBag = DisposeBag()
    
    var error = BehaviorRelay<String?>(value: nil)
    var errorName = BehaviorRelay<String?>(value: nil)
    var errorBio = BehaviorRelay<String?>(value: nil)
    var errorHometown = BehaviorRelay<String?>(value: nil)
    var errorBirthday = BehaviorRelay<String?>(value: nil)
    var errorCompanyName = BehaviorRelay<String?>(value: nil)
    var errorPositionName = BehaviorRelay<String?>(value: nil)
    var errorEntryDate = BehaviorRelay<String?>(value: nil)
    var errorExitDate = BehaviorRelay<String?>(value: nil)
    
    var postUpdateProfileResponse = BehaviorRelay<NetworkResponse?>(value: nil)
    var postUpdateProfileEduResponse = BehaviorRelay<NetworkResponse?>(value: nil)
    var postUpdateProfileCareerResponse = BehaviorRelay<NetworkResponse?>(value: nil)
    var uploadProfileImageResponse = BehaviorRelay<NetworkResponse?>(value: nil)
    
    var genderList = BehaviorRelay<[Item]>(value: [])
    var currentIndexGender = BehaviorRelay<Int>(value: 0)
    var isSuccess = BehaviorRelay<Bool?>(value: nil)
    var uploadData = BehaviorRelay<UserPictureModel?>(value: nil)
    
    required init() {
        super.init()
        registerObserver()
    }
    
    fileprivate func registerObserver() {
        postUpdateProfileResponse.subscribe(onNext: { [weak self] response in
            self?.isLoading.accept(false)
            
            guard let response = response else {  return }
            
            if response.isSuccess() {
                self?.handleResponseUpdate(networkResponse: response)
            } else {
                self?.error.accept(response.response as? String)
            }
        }).disposed(by: disposeBag)
        
        postUpdateProfileEduResponse.subscribe(onNext: { [weak self] response in
            self?.isLoading.accept(false)
            
            guard let response = response else {  return }
            
            if response.isSuccess() {
                self?.handleResponseUpdate(networkResponse: response)
            } else {
                self?.error.accept(response.response as? String)
            }
        }).disposed(by: disposeBag)
        
        postUpdateProfileCareerResponse.subscribe(onNext: { [weak self] response in
            self?.isLoading.accept(false)
            
            guard let response = response else {  return }
            
            if response.isSuccess() {
                self?.handleResponseUpdate(networkResponse: response)
            } else {
                self?.error.accept(response.response as? String)
            }
        }).disposed(by: disposeBag)
        
        uploadProfileImageResponse.subscribe(onNext: { [weak self] response in
            self?.isLoading.accept(false)
            
            guard let response = response else { return }
            if response.isSuccess() {
                self?.handleUploadImageResponse(networkResponse: response)
            } else {
                self?.error.accept(response.response as? String)
            }
        }).disposed(by: disposeBag)
    }
    
    fileprivate func handleUploadImageResponse(networkResponse: NetworkResponse) {
        let json = JSON(networkResponse.response!)
        if json["errors"].exists() {
            error.accept(json["errors"][0].stringValue)
        } else {
            uploadData.accept(UserPictureModel(json["user_picture"]))
        }
    }
    
    func setupGenderList() {
        let male = Item(text: "Laki-laki", value: 0)
        let female = Item(text: "Perempuan", value: 1)
        genderList.accept([male, female])
    }
    
    fileprivate func handleResponseUpdate(networkResponse: NetworkResponse) {
        let json = JSON(networkResponse.response!)
        if json["errors"].exists() {
            isSuccess.accept(false)
        } else {
            isSuccess.accept(true)
        }
    }
    
    func validateBio(bio: String?) -> Bool {
        if bio.ifNil("").isEmpty {
            errorBio.accept("Mohon masukkan bio kamu.")
        } else {
            errorBio.accept(nil)
            return true
        }
        
        return false
    }
    
    func validateHometown(hometown: String?) -> Bool {
        if hometown.ifNil("").isEmpty {
            errorHometown.accept("Mohon masukkan kota asal kamu.")
        } else {
            errorHometown.accept(nil)
            return true
        }
        
        return false
    }
    
    func validateBirthday(birthday: String?) -> Bool {
        if birthday.ifNil("").isEmpty {
            errorBirthday.accept("Mohon masukkan tanggal lahir.")
        } else {
            errorBirthday.accept(nil)
            return true
        }
        
        return false
    }
    
    func validateName(name: String?) -> Bool {
        if name.ifNil("").isEmpty {
            errorName.accept("Mohon masukkan nama.")
        } else if name.ifNil("").count < 2 {
            errorName.accept("Nama harus diisi minimal 2 karakter.")
        } else if !name.ifNil("").isAlphabet {
            errorName.accept("Nama tidak valid.")
        } else if name.ifNil("").count > 50 {
            errorName.accept("Nama tidak boleh lebih dari 50 karakter.")
        } else {
            errorName.accept(nil)
            return true
        }
        
        return false
    }
    
    func validateSchool(schoolName: String?) -> Bool {
        if schoolName.ifNil("").isEmpty {
            errorBio.accept("Mohon masukkan asal sekolah kamu.")
        } else {
            errorBio.accept(nil)
            return true
        }
        
        return false
    }
    
    func validateGraduation(graduation: String?) -> Bool {
        if graduation.ifNil("").isEmpty {
            errorBirthday.accept("Mohon masukkan tanggal kelulusan.")
        } else {
            errorBirthday.accept(nil)
            return true
        }
        
        return false
    }
    
    func setupGenderLabel(_ gender: Int) {
        if gender == 0 {
            currentIndexGender.accept(0)
        } else {
            currentIndexGender.accept(1)
        }
    }
    
    func validateCompanyName(company: String?) -> Bool {
        if company.ifNil("").isEmpty {
            errorCompanyName.accept("Mohon masukkan perusahaan.")
        } else if company.ifNil("").count < 2 {
            errorCompanyName.accept("Nama perusahaan harus diisi minimal 2 karakter.")
        } else if !company.ifNil("").isAlphabet {
            errorCompanyName.accept("Nama perusahaan tidak valid.")
        } else if company.ifNil("").count > 50 {
            errorCompanyName.accept("Nama perusahaan tidak boleh lebih dari 50 karakter.")
        } else {
            errorName.accept(nil)
            return true
        }
        
        return false
    }
    
    func validatePosition(position: String?) -> Bool {
        if position.ifNil("").isEmpty {
            errorPositionName.accept("Mohon masukkan posisi kamu.")
        } else {
            errorPositionName.accept(nil)
            return true
        }
        
        return false
    }
    
    func validateEntryDate(date: String?) -> Bool {
        if date.ifNil("").isEmpty {
            errorEntryDate.accept("Mohon masukkan tanggal masuk.")
        } else {
            errorEntryDate.accept(nil)
            return true
        }
        
        return false
    }
    
    func validateExitDate(date: String?) -> Bool {
        if date.ifNil("").isEmpty {
            errorExitDate.accept("Mohon masukkan tanggal kamu keluar.")
        } else {
            errorExitDate.accept(nil)
            return true
        }
        
        return false
    }
    
    func postUpdateProfile(postParams: [String: Any]) {
        isLoading.accept(true)
        ProfileDataSource.updateProfile(params: postParams, networkResponse: postUpdateProfileResponse)
    }
    
    func postUpdateProfileEdu(postParams: [String: Any]) {
        isLoading.accept(true)
        ProfileDataSource.updateProfileEdu(params: postParams, networkResponse: postUpdateProfileEduResponse)
    }
    
    func postUpdateProfileCareer(postParams: [String: Any]) {
        isLoading.accept(true)
        ProfileDataSource.updateProfileCareer(params: postParams, networkResponse: postUpdateProfileCareerResponse)
    }
    
    func uploadImage(_ image: UIImage) {
        isLoading.accept(true)
        ProfileDataSource.uploadProfileImage(image: image, networkResponse: uploadProfileImageResponse)
    }
}
