//
//  AuthViewModel.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 15/02/21.
//

import RxSwift
import RxCocoa
import SwiftyJSON

class AuthViewModel: NetworkViewModel {
    let disposeBag = DisposeBag()
    
    var error = BehaviorRelay<String?>(value: nil)
    var errorPhoneNumber = BehaviorRelay<String?>(value: nil)
    var errorPassword = BehaviorRelay<String?>(value: nil)
    var errorCountry = BehaviorRelay<String?>(value: nil)
    
    var loginResponse = BehaviorRelay<NetworkResponse?>(value: nil)
    var registrationResponse = BehaviorRelay<NetworkResponse?>(value: nil)
    var OtpResponse = BehaviorRelay<NetworkResponse?>(value: nil)
    var validateOtpResponse = BehaviorRelay<NetworkResponse?>(value: nil)
        
    var loginData = BehaviorRelay<UserToken?>(value: nil)
    var registrationData = BehaviorRelay<AuthModel?>(value: nil)
    var otpReqData = BehaviorRelay<AuthModel?>(value: nil)
    var validateOtpData = BehaviorRelay<UserToken?>(value: nil)
    
    required init() {
        super.init()
        registerObserver()
    }
    
    fileprivate func registerObserver() {
        loginResponse.subscribe(onNext: { [weak self] response in
            self?.isLoading.accept(false)
            guard let response = response else {  return }
            
            if response.isSuccess() {
                self?.handleLoginResponse(networkResponse: response)
            } else {
                self?.error.accept(response.response as? String)
            }
        }).disposed(by: disposeBag)
        
        registrationResponse.subscribe(onNext: { [weak self] response in
            self?.isLoading.accept(false)
            guard let response = response else {  return }
            
            if response.isSuccess() {
                self?.handleRegistrationResponse(networkResponse: response)
            } else {
                self?.error.accept(response.response as? String)
            }
        }).disposed(by: disposeBag)
        
        OtpResponse.subscribe(onNext: { [weak self] response in
            self?.isLoading.accept(false)
            guard let response = response else {  return }
            
            if response.isSuccess() {
                self?.handleOtpResponse(networkResponse: response)
            } else {
                self?.error.accept(response.response as? String)
            }
        }).disposed(by: disposeBag)
        
        validateOtpResponse.subscribe(onNext: { [weak self] response in
            self?.isLoading.accept(false)
            guard let response = response else {  return }
            
            if response.isSuccess() {
                self?.handleValidateOtpResponse(networkResponse: response)
            } else {
                self?.error.accept(response.response as? String)
            }
        }).disposed(by: disposeBag)
    }
    
    func handleLoginResponse(networkResponse: NetworkResponse) {
        let json = JSON(networkResponse.response!)
        if json["errors"].exists() {
            error.accept(json["errors"][0].stringValue)
        } else {
            loginData.accept(UserToken(json["user"]))
        }
    }
    
    func handleRegistrationResponse(networkResponse: NetworkResponse) {
        let json = JSON(networkResponse.response!)
        if json["errors"].exists() {
            error.accept(json["errors"][0].stringValue)
        } else {
            registrationData.accept(AuthModel(json["user"]))
        }
    }
    
    func handleOtpResponse(networkResponse: NetworkResponse) {
        let json = JSON(networkResponse.response!)
        otpReqData.accept(AuthModel(json["user"]))
    }
    
    func handleValidateOtpResponse(networkResponse: NetworkResponse) {
        let json = JSON(networkResponse.response!)
        if !json["user"].isEmpty {
            validateOtpData.accept(UserToken(json["user"]))
        } else {
            error.accept("User tidak ditemukan.")
        }
    }
    
    func validatePhone(phone: String?) -> Bool {
        if phone.ifNil("").isEmpty {
            errorPhoneNumber.accept("Mohon masukkan nomor HP.")
        } else if !Helper.isValidPhone(phone.ifNil("")) {
            errorPhoneNumber.accept("Mohon masukkan nomor HP yang valid.")
        } else {
            errorPhoneNumber.accept(nil)
            return true
        }
        
        return false
    }
    
    func validatePassword(password: String?) -> Bool {
        if password.ifNil("").isEmpty {
            errorPassword.accept("Mohon masukkan password kamu.")
        } else {
            errorPassword.accept(nil)
            return true
        }
        
        return false
    }
    
    func validateCountry(country: String?) -> Bool {
        if country.ifNil("").isEmpty {
            errorCountry.accept("Mohon masukkan nama negara kamu.")
        } else {
            errorCountry.accept(nil)
            return true
        }
        
        return false
    }
    
    func registrationRequest(postParams: [String: Any]) {
        isLoading.accept(true)
        AuthDataSource.registrationRequest(params: postParams, networkResponse: registrationResponse)
    }
    
    func loginRequest(postParams: [String: Any]) {
        isLoading.accept(true)
        AuthDataSource.loginRequest(params: postParams, networkResponse: loginResponse)
    }
    
    func sendOtp(postParams: [String: Any]) {
        isLoading.accept(true)
        AuthDataSource.otpRequest(params: postParams, networkResponse: OtpResponse)
    }
    
    func validateOtp(postParams: [String: Any]) {
        isLoading.accept(true)
        AuthDataSource.validateOtp(params: postParams, networkResponse: validateOtpResponse)
    }
}
