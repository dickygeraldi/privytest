//
//  ProfileViewModel.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 17/02/21.
//

import RxSwift
import RxCocoa
import SwiftyJSON

class ProfileViewModel: NetworkViewModel {
    let disposeBag = DisposeBag()
    
    var error = BehaviorRelay<String?>(value: nil)
    
    var getProfileResponse = BehaviorRelay<NetworkResponse?>(value: nil)
    var uploadCoverImageResponse = BehaviorRelay<NetworkResponse?>(value: nil)
    
    var profileData = BehaviorRelay<ProfileModels?>(value: nil)
    var uploadData = BehaviorRelay<UserPictureModel?>(value: nil)
    var isSuccess = BehaviorRelay<Bool?>(value: nil)
    
    required init() {
        super.init()
        registerObserver()
    }
    
    fileprivate func registerObserver() {
        getProfileResponse.subscribe(onNext: { [weak self] response in
            self?.isLoading.accept(false)
            
            guard let response = response else {  return }
            
            if response.isSuccess() {
                self?.handleProfileResponse(networkResponse: response)
            } else {
                self?.error.accept(response.response as? String)
            }
        }).disposed(by: disposeBag)
        
        uploadCoverImageResponse.subscribe(onNext: { [weak self] response in
            self?.isLoading.accept(false)
            
            guard let response = response else { return }
            if response.isSuccess() {
                self?.handleUploadImageResponse(networkResponse: response)
            } else {
                self?.error.accept(response.response as? String)
            }
        }).disposed(by: disposeBag)
    }
    
    fileprivate func handleUploadImageResponse(networkResponse: NetworkResponse) {
        let json = JSON(networkResponse.response!)
        if json["errors"].exists() {
            error.accept(json["errors"][0].stringValue)
        } else {
            uploadData.accept(UserPictureModel(json["user_picture"]))
        }
    }
    
    fileprivate func handleProfileResponse(networkResponse: NetworkResponse) {
        let json = JSON(networkResponse.response!)
        if json["errors"].exists() {
            error.accept(json["errors"][0].stringValue)
        } else {
            profileData.accept(ProfileModels(json["user"]))
        }
    }
    
    fileprivate func handleResponseUpdate(networkResponse: NetworkResponse) {
        let json = JSON(networkResponse.response!)
        if json["errors"].exists() {
            isSuccess.accept(false)
        } else {
            isSuccess.accept(true)
        }
    }
    
    func getProfile() {
        isLoading.accept(true)
        ProfileDataSource.getProfile(networkResponse: getProfileResponse)
    }
    
    func uploadImage(_ image: UIImage) {
        isLoading.accept(true)
        ProfileDataSource.uploadImage(image: image, networkResponse: uploadCoverImageResponse)
    }
}
