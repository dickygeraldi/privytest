//
//  String+Ext.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 16/02/21.
//

import Foundation

extension String {
    func maskPhoneNumber() -> String {
        let phoneNumber = "\(self.dropLast(3))XXX"
        let phoneNumbers = Array(phoneNumber)
        var maskPhoneNumber = ""
        for item in 0..<phoneNumbers.count {
            if item > 0 && item % 3 == 0 {
                maskPhoneNumber.append("-\(phoneNumbers[item])")
            } else {
                maskPhoneNumber.append(phoneNumbers[item])
            }
        }
        return maskPhoneNumber
    }

    var isAlphabet: Bool {
        let ALPHABET_REGEX = "([a-zA-Z'-]+( [a-zA-Z'-]+)*)"
        let alphabetTest = NSPredicate(format: "SELF MATCHES %@", ALPHABET_REGEX)
        let result = alphabetTest.evaluate(with: self)

        return result
    }
}
