//
//  Validation.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 16/02/21.
//

import Foundation

struct Helper {
    static func isValidPhone(_ value: String) -> Bool {
        let PHONE_REGEX = "^[0][8][0-9]{8,12}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result = phoneTest.evaluate(with: value)

        return result
    }
}
