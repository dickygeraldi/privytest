//
//  GenderCollectionViewCell.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 19/02/21.
//

import UIKit

class GenderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    var filter: String! {
        didSet {
            self.bindView()
        }
    }
    
    func bindView() {
        titleLabel.text = filter
    }
    
    func selectedFilter() {
        cellView.layer.borderColor = UIColor.green.cgColor
        titleLabel.textColor = .black
    }
    
    func deselectedFilter() {
        cellView.layer.borderColor = UIColor.snow().cgColor
        titleLabel.textColor = .black
    }
    
    func reset() {
        cellView.layer.borderWidth = 1
        cellView.layer.borderColor = UIColor.green.cgColor
    }
}
