//
//  ProfilePhotoCollectionViewCell.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 19/02/21.
//

import UIKit

class ProfilePhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imagePhoto: UIImageView!
    
    var url: String! {
        didSet {
            self.bindView()
        }
    }
    
    func bindView() {
        if let url = URL(string: url) {
            imagePhoto.download(from: url)
        }
    }
}
