//
//  EditProfileViewController.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 19/02/21.
//

import UIKit
import SwifterSwift

class EditProfileViewController: BaseNetworkViewController<EditProfileViewModel> {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var errorNameLabel: UILabel!
    @IBOutlet weak var genderCollectionView: UICollectionView!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var birthdayErrorLabel: UILabel!
    @IBOutlet weak var hometownTextField: UITextField!
    @IBOutlet weak var errorHometown: UILabel!
    @IBOutlet weak var bioTextField: UITextField!
    @IBOutlet weak var bioErrorLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var photoProfileImage: UIImageView!
    
    var data: ProfileModels? = nil
    let pickBirthday = UIDatePicker()
    let locale = Locale.preferredLanguages.first
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        setupNavbar()
        setupView()
        setupCollectionView()
        setupTextField()
    }
    
    override func registerObserver() {
        viewModel?.genderList.subscribe(onNext: { [weak self] genderList in
            if !genderList.isEmpty {
                self?.genderCollectionView.reloadData()
            }
        }).disposed(by: disposeBag)
        
        viewModel?.errorName.subscribe(onNext: { [weak self] (errorName) in
            guard let self = self else {
                return
            }
            self.errorNameLabel?.isHidden = errorName.isNilOrEmpty
            self.errorNameLabel?.text = errorName
        }).disposed(by: disposeBag)
        
        viewModel?.errorBio.subscribe(onNext: { [weak self] (errorName) in
            guard let self = self else {
                return
            }
            self.bioErrorLabel?.isHidden = errorName.isNilOrEmpty
            self.bioErrorLabel?.text = errorName
        }).disposed(by: disposeBag)
        
        viewModel?.errorBirthday.subscribe(onNext: { [weak self] (errorName) in
            guard let self = self else {
                return
            }
            self.birthdayErrorLabel?.isHidden = errorName.isNilOrEmpty
            self.birthdayErrorLabel?.text = errorName
        }).disposed(by: disposeBag)
        
        viewModel?.errorHometown.subscribe(onNext: { [weak self] (errorName) in
            guard let self = self else {
                return
            }
            self.errorHometown?.isHidden = errorName.isNilOrEmpty
            self.errorHometown?.text = errorName
        }).disposed(by: disposeBag)
        
        viewModel?.currentIndexGender.subscribe(onNext: { [weak self] currentIndexGender in
            self?.genderCollectionView.reloadData()
        }).disposed(by: disposeBag)
        
        viewModel?.error.subscribe(onNext: { (message) in
            self.loadingView.stopAnimating()
            
            self.view.makeToast(message)
        }).disposed(by: disposeBag)
        
        viewModel?.isSuccess.subscribe(onNext: { [weak self] isSuccess in
            guard let vc = self, let isSuccess = isSuccess else {
                return
            }
            
            vc.loadingView.stopAnimating()
            
            if isSuccess {
                vc.view.makeToast("Data berhasil diupdate")
                vc.navigationController?.popViewController()
            }
        }).disposed(by: disposeBag)
        
        viewModel?.uploadData.subscribe(onNext: { [weak self] data in
            guard let data = data, let vc = self else {
                return
            }
            
            vc.loadingView.stopAnimating()
            if data.id.isEmpty {
                vc.view.makeToast("Gagal upload photo.")
            } else {
                vc.view.makeToast("Upload image berhasil.")
            }
        }).disposed(by: disposeBag)
    }
    
    @IBAction func submitProfile(_ sender: Any) {
        sendData()
    }
    
    @objc func back(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated:true)
    }
    
    @objc func onEditingName() {
        _ = viewModel?.validateName(name: nameTextField.text)
    }
    
    @objc func onEditingBirthday() {
        _ = viewModel?.validateBirthday(birthday: birthdayTextField.text)
    }
    
    @objc func onEditingHometown() {
        _ = viewModel?.validateHometown(hometown: hometownTextField.text)
    }
    
    @objc func onEditingBio() {
        _ = viewModel?.validateBio(bio: bioTextField.text)
    }
    
    @objc func pickBirthdayChange() {
        getDateFromPicker(sender: pickBirthday)
    }
}

extension EditProfileViewController {
    fileprivate func getDateFromPicker(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        birthdayTextField.text = formatter.string(from: pickBirthday.date)
    }
    
    fileprivate func setupTextField() {
        birthdayTextField.inputView = pickBirthday
        pickBirthday.datePickerMode = .date
        pickBirthday.locale = Locale(identifier: locale!)
        pickBirthday.maximumDate = Date()
        
        pickBirthday.addTarget(self, action: #selector(pickBirthdayChange), for: .valueChanged)
        nameTextField.addTarget(self, action: #selector(onEditingName), for: .editingChanged)
        birthdayTextField.addTarget(self, action: #selector(onEditingBirthday), for: .editingChanged)
        hometownTextField.addTarget(self, action: #selector(onEditingHometown), for: .editingChanged)
        bioTextField.addTarget(self, action: #selector(onEditingBio), for: .editingChanged)
    }
    
    fileprivate func setupCollectionView() {
        genderCollectionView.register(nibWithCellClass: GenderCollectionViewCell.self)
        genderCollectionView.backgroundColor = .white
        genderCollectionView.dataSource = self
        genderCollectionView.delegate = self
        
        viewModel?.setupGenderList()
    }
    
    fileprivate func setupView() {
        guard let data = data else {
            return
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(updateImage))
        
        nameTextField.text = data.name
        birthdayTextField.text = data.birthday
        hometownTextField.text = data.hometown
        bioTextField.text = data.bio
        photoProfileImage.addGestureRecognizer(tap)
        
        errorHometown.isHidden = true
        errorNameLabel.isHidden = true
        birthdayErrorLabel.isHidden = true
        bioErrorLabel.isHidden = true
        
        photoProfileImage.layer.masksToBounds = true
        submitButton.cornerRadius = 10
    }
    
    fileprivate func setupNavbar() {
        let btnLeftMenu: UIButton = UIButton()
        btnLeftMenu.setImage(UIImage(named: "icon_back"), for: UIControl.State())
        btnLeftMenu.addTarget(self, action: #selector(back(sender:)), for: UIControl.Event.touchUpInside)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    fileprivate func sendData() {
        guard let viewModel = viewModel else {
            return
        }

        let isValidName = viewModel.validateName(name: nameTextField.text)
        let isValidBio = viewModel.validateBio(bio: bioTextField.text)
        let isValidBirthday = viewModel.validateBirthday(birthday: birthdayTextField.text)
        let isValidHometown = viewModel.validateHometown(hometown: hometownTextField.text)
        
        if isValidBio, isValidName, isValidBirthday, isValidHometown {
            self.loadingView.startAnimating()

            var postParams: [String: Any] = [:]
            postParams["name"] = nameTextField.text
            postParams["birthday"] = birthdayTextField.text
            postParams["hometown"] = hometownTextField.text
            postParams["bio"] = bioTextField.text
            postParams["gender"] = viewModel.currentIndexGender.value
            
            viewModel.uploadImage(photoProfileImage.image!)
            viewModel.postUpdateProfile(postParams: postParams)
        }
    }
}

extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @objc func updateImage() {
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = false
        
        self.present(image, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            photoProfileImage.image = image
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension EditProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let viewModel = viewModel else {
            return 0
        }
        
        return viewModel.genderList.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let viewModel = viewModel else {
            return UICollectionViewCell()
        }
        
        let cell = collectionView.dequeueReusableCell(withClass: GenderCollectionViewCell.self, for: indexPath)
        cell.filter = viewModel.genderList.value[safe: indexPath.row]?.text
        if indexPath.row == viewModel.currentIndexGender.value {
            cell.selectedFilter()
        } else {
            cell.deselectedFilter()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let viewModel = viewModel else {
            return
        }
        
        viewModel.currentIndexGender.accept(indexPath.row)
    }
}
