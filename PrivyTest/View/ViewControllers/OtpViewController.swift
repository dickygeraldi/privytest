//
//  OtpViewController.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 16/02/21.
//

import UIKit
import OTPFieldView

class OtpViewController: BaseNetworkViewController<AuthViewModel> {

    
    @IBOutlet weak var numberPhoneLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var resendOtpLabel: UILabel!
    @IBOutlet weak var otpTextField: OTPFieldView!
    @IBOutlet weak var submitButton: UIButton!
    
    var counter = 60
    var phoneNumber: String = ""
    var otpString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavbar()
        setupView()
        setupTimer()
        sendReqOtp()
    }
    
    override func registerObserver() {
        viewModel?.error.subscribe(onNext: { (message) in
            self.loadingView.stopAnimating()
            
            self.view.makeToast(message)
        }).disposed(by: disposeBag)
        
        viewModel?.otpReqData.subscribe(onNext: { [weak self] data in
            guard let vc = self else {
                return
            }
            
            vc.loadingView.stopAnimating()
            self?.view.makeToast("OTP Sedang dikirim")
        }).disposed(by: disposeBag)
        
        viewModel?.validateOtpData.subscribe(onNext: { [weak self] data in
            guard let vc = self, let data = data else {
                return
            }
            
            vc.loadingView.stopAnimating()
            if !data.accessToken.isEmpty {
                PreferenceService.saveLoginStatus(true)
                PreferenceService.saveAuthToken(data.accessToken)
                
                let storyboard = UIStoryboard(name: "ProfileViewController", bundle: nil)
                let controller = storyboard.instantiateViewController(identifier: "ProfileViewController") as? ProfileViewController
                
                vc.navigationController?.pushViewController(controller!, animated: true)
            } else {
                self?.view.makeToast("OTP tidak valid.")
            }
        }).disposed(by: disposeBag)
    }
    
    @objc func back(sender: UIBarButtonItem) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func resendOtp() {
        counter = 60
        setupTimer()
        sendReqOtp()
    }
    
    @IBAction func validateOtp(_ sender: UIButton) {
        sendOtp(otpString)
    }
}

extension OtpViewController {
    fileprivate func sendReqOtp() {
        var postParams: [String: Any] = [:]
        postParams["phone"] = phoneNumber
        
        viewModel?.sendOtp(postParams: postParams)
    }
    
    fileprivate func setupNavbar() {
        let btnLeftMenu: UIButton = UIButton()
        btnLeftMenu.setImage(UIImage(named: "icon_back"), for: UIControl.State())
        btnLeftMenu.addTarget(self, action: #selector(back(sender:)), for: UIControl.Event.touchUpInside)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    fileprivate func setupView() {
        setupOtpView()
        
        numberPhoneLabel.text = phoneNumber.maskPhoneNumber()
        submitButton.isUserInteractionEnabled = false
        submitButton.alpha = 0.5
    }
    
    fileprivate func setupOtpView() {
        self.otpTextField.fieldsCount = 4
        self.otpTextField.fieldBorderWidth = 2
        self.otpTextField.defaultBorderColor = UIColor.black
        self.otpTextField.filledBorderColor = UIColor.green
        self.otpTextField.cursorColor = UIColor.red
        self.otpTextField.displayType = .underlinedBottom
        self.otpTextField.fieldSize = 40
        self.otpTextField.separatorSpace = 8
        self.otpTextField.shouldAllowIntermediateEditing = false
        self.otpTextField.delegate = self
        self.otpTextField.initializeUI()
    }
    
    fileprivate func setupTimer() {
        resendOtpLabel.isHidden = true
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
            if self.counter > 0 {
                self.timerLabel.text = "00:\(self.counter)"
                self.counter -= 1
            } else {
                self.timerLabel.isHidden = true
                self.resendOtpLabel.isHidden = false
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.resendOtp))
                self.resendOtpLabel.addGestureRecognizer(tap)
                Timer.invalidate()
            }
        }
    }
    
    fileprivate func sendOtp(_ otpString: String) {
        guard let data = viewModel?.otpReqData.value else { return }
        
        var postParams : [String: Any] = [:]
        postParams["user_id"] = data.id
        postParams["otp_code"] = otpString
        
        viewModel?.validateOtp(postParams: postParams)
    }
}

extension OtpViewController: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        
        if hasEntered {
            submitButton.isUserInteractionEnabled = true
            submitButton.alpha = 1.0
            submitButton.addTarget(self, action: #selector(validateOtp(_:)), for: .touchUpInside)
        }
        
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        self.otpString = otpString
        
        sendOtp(otpString)
    }
}
