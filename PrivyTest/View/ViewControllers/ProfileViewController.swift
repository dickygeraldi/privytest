//
//  ProfileViewController.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 17/02/21.
//

import UIKit

class ProfileViewController: BaseNetworkViewController<ProfileViewModel> {

    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var homeTownLabel: UILabel!
    @IBOutlet weak var biografiLabel: UILabel!
    @IBOutlet weak var educationHistoryLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var zodiacLabel: UILabel!
    @IBOutlet weak var carierHistoryLabel: UILabel!
    @IBOutlet weak var editProfile: UIButton!
    @IBOutlet weak var addEducationButton: UIButton!
    @IBOutlet weak var addCareerButton: UIButton!
    @IBOutlet weak var galleryCollection: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        
        loadingView.startAnimating()
        setupNavbar(hasScroll: false)
        setupImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if PreferenceService.isLogin() {
            viewModel?.getProfile()
        } else {
            let storyboard = UIStoryboard(name: "LoginViewController", bundle: nil)
            let controller = storyboard.instantiateViewController(identifier: "LoginViewController")
            controller.modalPresentationStyle = .overCurrentContext
            self.present(controller, animated: true)
        }
    }
    
    override func registerObserver() {
        viewModel?.profileData.subscribe(onNext: { [weak self] data in
            guard let data = data, let vc = self else {
                return
            }
            
            vc.loadingView.stopAnimating()
            vc.galleryCollection.reloadData()
            vc.setupView(data: data)
        }).disposed(by: disposeBag)
        
        viewModel?.error.subscribe(onNext: { (message) in
            self.loadingView.stopAnimating()
            
            self.view.makeToast(message)
        }).disposed(by: disposeBag)
        
        viewModel?.uploadData.subscribe(onNext: { [weak self] data in
            guard let data = data, let vc = self else {
                return
            }
            
            vc.loadingView.stopAnimating()
            if data.id.isEmpty {
                vc.view.makeToast("Gagal upload photo.")
            } else {
                if let url = URL(string: data.url) {
                    vc.coverImage.download(from: url)
                }
                vc.view.makeToast("Upload image berhasil.")
            }
        }).disposed(by: disposeBag)
    }
    
    @IBAction func goToEditProfile(_ sender: Any) {
        guard let data = viewModel?.profileData.value else { return }
        
        let storyboard = UIStoryboard(name: "EditProfileViewController", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "EditProfileViewController") as? EditProfileViewController
        controller?.data = data
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    @IBAction func goToEdu(_ sender: Any) {
        let storyboard = UIStoryboard(name: "AddEducationHistory", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "AddEducationViewController")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func goToCareer(_ sender: Any) {
        let storyboard = UIStoryboard(name: "AddCareerHistory", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "AddCareerViewController")
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension ProfileViewController {
    fileprivate func setupView(data: ProfileModels) {
        guard let careerData = data.career else { return }
        guard let educationData = data.education else { return }
        guard let userPictureData = data.userPicture else { return }
        let tap = UITapGestureRecognizer(target: self, action: #selector(updateImage))
        
        nameLabel.text = data.name
        homeTownLabel.text = data.hometown
        biografiLabel.text = data.bio
        ageLabel.text = "\(data.age) tahun"
        genderLabel.text = "\(data.gender)"
        birthdayLabel.text = data.birthday
        zodiacLabel.text = data.zodiac
        coverImage.addGestureRecognizer(tap)
        galleryCollection.dataSource = self
        galleryCollection.delegate = self
        galleryCollection.register(nibWithCellClass: ProfilePhotoCollectionViewCell.self)
        
        educationHistoryLabel.text = "\(educationData.schollName) - \(educationData.graduationTime)"
        carierHistoryLabel.text = "\(careerData.companyName) - \(careerData.startFrom) to \(careerData.endIn)"
        editProfile.layer.cornerRadius = 10
        addEducationButton.layer.cornerRadius = 10
        addCareerButton.layer.cornerRadius = 10
        
        if userPictureData.url.isEmpty {
            avatarImage.image = UIImage(named: "avatarDefault")
        } else {
            if let url = URL(string: userPictureData.url) {
                avatarImage.download(from: url)
            }
        }
        
        if data.coverPicture.isEmpty {
            coverImage.image = UIImage(named: "coverDefault")
        } else {
            if let url = URL(string: data.coverPicture) {
                coverImage.download(from: url)
            }
        }
    }
    
    fileprivate func setupImage() {
        avatarImage.layer.masksToBounds = true
        avatarImage.layer.cornerRadius = avatarImage.bounds.width / 2
    }
    
    fileprivate func setupNavbar(hasScroll: Bool) {
        if hasScroll {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController?.navigationBar.backgroundColor = .white
            self.navigationItem.setHidesBackButton(true, animated: true)
            self.title = "Profile Kamu"
        } else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }
}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @objc func updateImage() {
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = false
        
        self.present(image, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            viewModel?.uploadImage(image)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension ProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let data = viewModel?.profileData.value else {
            return 0
        }
        
        return data.userPictures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let data = viewModel?.profileData.value else {
            return UICollectionViewCell()
        }
        
        let cell = galleryCollection.dequeueReusableCell(withClass: ProfilePhotoCollectionViewCell.self, for: indexPath)
        cell.url = data.userPictures[indexPath.row]?.url
        
        return cell
    }
}

extension ProfileViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y / 20
        if offset > 3.6 {
            UIView.animate(withDuration: 0.2, animations: {
                self.setupNavbar(hasScroll: true)
            })
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.setupNavbar(hasScroll: false)
            })
        }
    }
}
