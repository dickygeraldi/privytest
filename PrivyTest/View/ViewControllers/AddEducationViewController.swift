//
//  AddEducationViewController.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 19/02/21.
//

import UIKit

class AddEducationViewController: BaseNetworkViewController<EditProfileViewModel> {

    @IBOutlet weak var schoolNameTextField: UITextField!
    @IBOutlet weak var graduationDateTextField: UITextField!
    @IBOutlet weak var errorSchoolLabel: UILabel!
    @IBOutlet weak var errorGraduationLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    
    let pickGraduation = UIDatePicker()
    let locale = Locale.preferredLanguages.first
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        setupView()
        setupNavbar()
        setupTextField()
    }
    
    override func registerObserver() {
        viewModel?.errorBio.subscribe(onNext: { [weak self] (errorName) in
            guard let self = self else {
                return
            }
            self.errorSchoolLabel?.isHidden = errorName.isNilOrEmpty
            self.errorSchoolLabel?.text = errorName
        }).disposed(by: disposeBag)
        
        viewModel?.errorBirthday.subscribe(onNext: { [weak self] (errorName) in
            guard let self = self else {
                return
            }
            self.errorGraduationLabel?.isHidden = errorName.isNilOrEmpty
            self.errorGraduationLabel?.text = errorName
        }).disposed(by: disposeBag)
        
        viewModel?.error.subscribe(onNext: { (message) in
            self.loadingView.stopAnimating()
            
            self.view.makeToast(message)
        }).disposed(by: disposeBag)
        
        viewModel?.isSuccess.subscribe(onNext: { [weak self] isSuccess in
            guard let vc = self, let isSuccess = isSuccess else {
                return
            }
            
            vc.loadingView.stopAnimating()
            
            if isSuccess {
                vc.view.makeToast("Data berhasil diupdate")
                vc.navigationController?.popViewController()
            }
        }).disposed(by: disposeBag)
    }
    
    @IBAction func submitData(_ sender: Any) {
        sendData()
    }
    
    @objc func back(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated:true)
    }
    
    @objc func pickGraduationDate() {
        getDateFromPicker(sender: pickGraduation)
    }
    
    @objc func onEditingSchoolName() {
        _ = viewModel?.validateBio(bio: schoolNameTextField.text)
    }
    
    @objc func onEditingGraduation() {
        _ = viewModel?.validateBirthday(birthday: graduationDateTextField.text)
    }
}

extension AddEducationViewController {
    fileprivate func getDateFromPicker(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        graduationDateTextField.text = formatter.string(from: pickGraduation.date)
    }
    
    fileprivate func setupNavbar() {
        let btnLeftMenu: UIButton = UIButton()
        btnLeftMenu.setImage(UIImage(named: "icon_back"), for: UIControl.State())
        btnLeftMenu.addTarget(self, action: #selector(back(sender:)), for: UIControl.Event.touchUpInside)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    fileprivate func setupView() {
        errorSchoolLabel.isHidden = true
        errorGraduationLabel.isHidden = true
        submitButton.layer.cornerRadius = 10
    }
    
    fileprivate func setupTextField() {
        graduationDateTextField.inputView = pickGraduation
        pickGraduation.datePickerMode = .date
        pickGraduation.locale = Locale(identifier: locale!)
        pickGraduation.maximumDate = Date()
        
        pickGraduation.addTarget(self, action: #selector(pickGraduationDate), for: .valueChanged)
        schoolNameTextField.addTarget(self, action: #selector(onEditingSchoolName), for: .valueChanged)
        graduationDateTextField.addTarget(self, action: #selector(onEditingGraduation), for: .editingChanged)
    }
    
    fileprivate func sendData() {
        guard let viewModel = viewModel else {
            return
        }
        
        let isValidSchool = viewModel.validateSchool(schoolName: schoolNameTextField.text)
        let isValidGraduation = viewModel.validateGraduation(graduation: graduationDateTextField.text)
        
        if isValidSchool, isValidGraduation {
            self.loadingView.startAnimating()
            
            var postParams: [String: Any] = [:]
            postParams["school_name"] = schoolNameTextField.text
            postParams["graduation_time"] = graduationDateTextField.text
            
            viewModel.postUpdateProfileEdu(postParams: postParams)
        }
    }
}
