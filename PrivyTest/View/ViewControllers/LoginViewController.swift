//
//  LoginViewController.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 15/02/21.
//

import UIKit
import RxSwift
import MapKit
import Toast_Swift

class LoginViewController: BaseNetworkViewController<AuthViewModel> {
    
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var footerLabel: UILabel!
    @IBOutlet weak var phoneErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var registrationClick: UILabel!
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupNavbar()
        setupTextField()
        
        locManager.requestWhenInUseAuthorization()
    }
    
    override func registerObserver() {
        viewModel?.error.subscribe(onNext: { (message) in
            self.loadingView.stopAnimating()
            
            self.view.makeToast(message)
        }).disposed(by: disposeBag)
        
        viewModel?.errorPhoneNumber.subscribe(onNext: { [weak self] errorMessage in
            guard let vc = self else {
                return
            }
            
            vc.phoneErrorLabel.isHidden = errorMessage.ifNil("").isEmpty
            vc.phoneErrorLabel.text = errorMessage.ifNil("")
        }).disposed(by: disposeBag)
        
        viewModel?.errorPassword.subscribe(onNext: { [weak self] errorMessage in
            guard let vc = self else {
                return
            }
            
            vc.passwordErrorLabel.isHidden = errorMessage.ifNil("").isEmpty
            vc.passwordErrorLabel.text = errorMessage.ifNil("")
        }).disposed(by: disposeBag)
        
        viewModel?.loginData.subscribe(onNext: { [weak self] data in
            guard let vc = self, let data = data else {
                return
            }
            
            vc.loadingView.stopAnimating()
            if !data.accessToken.isEmpty {
                PreferenceService.saveLoginStatus(true)
                PreferenceService.saveAuthToken(data.accessToken)
                
                let storyboard = UIStoryboard(name: "ProfileViewController", bundle: nil)
                let controller = storyboard.instantiateViewController(identifier: "ProfileViewController")
                vc.navigationController?.pushViewController(controller, animated: true)
            } else {
                self?.view.makeToast("Nomor Hp atau password salah.")
            }
        }).disposed(by: disposeBag)
    }
    
    fileprivate func setupView() {
        phoneTextField.setLeftPaddingPoints(10)
        passwordTextField.setLeftPaddingPoints(10)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(toRegistration))
        registrationClick.addGestureRecognizer(tap)
        submitButton.layer.cornerRadius = 10
        phoneErrorLabel.isHidden = true
        passwordErrorLabel.isHidden = true
    }
    
    @objc func toRegistration() {
        let storyboard = UIStoryboard(name: "RegistrationViewController", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "RegistrationViewController")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func submitData(_ sender: Any) {
        sendRequest()
    }
    
    fileprivate func sendRequest() {
        guard let viewModel = viewModel else {
            return
        }

        let isValidPhone = viewModel.validatePhone(phone: phoneTextField.text)
        let isValidPass = viewModel.validatePassword(password: passwordTextField.text)

        if isValidPass, isValidPhone {
            self.loadingView.startAnimating()

            var postParams: [String: Any] = [:]
            postParams["phone"] = phoneTextField.text?.replacingOccurrences(of: "0", with: "62")
            postParams["password"] = passwordTextField.text
            postParams["latlong"] = getLonLat()
            postParams["device_token"] = PreferenceService.getDeviceKey()
            postParams["device_type"] = 0
            viewModel.loginRequest(postParams: postParams)
        }
    }
    
    fileprivate func getLonLat() -> String {
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            guard let currentLocation = locManager.location else {
                return ""
            }
            
            return "\(currentLocation.coordinate.latitude),\(currentLocation.coordinate.longitude)"
        } else {
            return "123"
        }
    }
    
    fileprivate func setupNavbar(_ title: String = "") {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    func setupTextField() {
        passwordTextField.addTarget(self, action: #selector(onEditingPassword), for: .editingChanged)
        phoneTextField.addTarget(self, action: #selector(onEditingPhoneNo), for: .editingChanged)
    }
    
    @objc func onEditingPhoneNo() {
        _ = viewModel?.validatePhone(phone: phoneTextField.text)
    }
    
    @objc func onEditingPassword() {
        _ = viewModel?.validatePassword(password: passwordTextField.text)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTextField {
            let characterSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: characterSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        } else {
            return string.count > 0
        }
    }
}
