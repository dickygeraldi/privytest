//
//  AddCareerViewController.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 19/02/21.
//

import UIKit

class AddCareerViewController: BaseNetworkViewController<EditProfileViewModel> {

    @IBOutlet weak var companyNameTextField: UITextField!
    @IBOutlet weak var errorCompanyLabel: UILabel!
    @IBOutlet weak var positionTextField: UITextField!
    @IBOutlet weak var positionErrorLabel: UILabel!
    @IBOutlet weak var entryDateTextField: UITextField!
    @IBOutlet weak var entryDateErrorLabel: UILabel!
    @IBOutlet weak var exitDateTextField: UITextField!
    @IBOutlet weak var exitDateTextLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    
    let pickEntryDate = UIDatePicker()
    let pickExitDate = UIDatePicker()
    let locale = Locale.preferredLanguages.first
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        setupNavbar()
        setupView()
        setupTextField()
    }
    
    override func registerObserver() {
        viewModel?.errorCompanyName.subscribe(onNext: { [weak self] (errorName) in
            guard let self = self else {
                return
            }
            self.errorCompanyLabel?.isHidden = errorName.isNilOrEmpty
            self.errorCompanyLabel?.text = errorName
        }).disposed(by: disposeBag)
        
        viewModel?.errorPositionName.subscribe(onNext: { [weak self] (errorName) in
            guard let self = self else {
                return
            }
            self.positionErrorLabel.isHidden = errorName.isNilOrEmpty
            self.positionErrorLabel?.text = errorName
        }).disposed(by: disposeBag)
        
        viewModel?.errorEntryDate.subscribe(onNext: { [weak self] (errorName) in
            guard let self = self else {
                return
            }
            self.entryDateErrorLabel?.isHidden = errorName.isNilOrEmpty
            self.entryDateErrorLabel?.text = errorName
        }).disposed(by: disposeBag)
        
        viewModel?.errorExitDate.subscribe(onNext: { [weak self] (errorName) in
            guard let self = self else {
                return
            }
            self.exitDateTextLabel?.isHidden = errorName.isNilOrEmpty
            self.exitDateTextLabel?.text = errorName
        }).disposed(by: disposeBag)
        
        viewModel?.error.subscribe(onNext: { (message) in
            self.loadingView.stopAnimating()
            
            self.view.makeToast(message)
        }).disposed(by: disposeBag)
        
        viewModel?.isSuccess.subscribe(onNext: { [weak self] isSuccess in
            guard let vc = self, let isSuccess = isSuccess else {
                return
            }
            
            vc.loadingView.stopAnimating()
            
            if isSuccess {
                vc.view.makeToast("Data berhasil diupdate")
                vc.navigationController?.popViewController()
            }
        }).disposed(by: disposeBag)
    }
    
    @IBAction func submitData(_ sender: Any) {
        sendData()
    }
    
    @objc func back(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated:true)
    }
    
    @objc func onEditPickEntryDate() {
        getDateFromPicker(sender: pickEntryDate)
    }
    
    @objc func onEditPickExitDate() {
        getDateFromPicker(sender: pickExitDate)
    }
    
    @objc func onEditingCompanyName() {
        _ = viewModel?.validateCompanyName(company: companyNameTextField.text)
    }
    
    @objc func onEditingPosition() {
        _ = viewModel?.validatePosition(position: positionTextField.text)
    }
    
    @objc func onEditingEntryDate() {
        _ = viewModel?.validateEntryDate(date: entryDateTextField.text)
    }
    
    @objc func onEditingExitDate() {
        _ = viewModel?.validateExitDate(date: exitDateTextField.text)
    }
}

extension AddCareerViewController {
    fileprivate func setupNavbar() {
        let btnLeftMenu: UIButton = UIButton()
        btnLeftMenu.setImage(UIImage(named: "icon_back"), for: UIControl.State())
        btnLeftMenu.addTarget(self, action: #selector(back(sender:)), for: UIControl.Event.touchUpInside)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func getDateFromPicker(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
                
        if sender == pickEntryDate {
            entryDateTextField.text = formatter.string(from: pickEntryDate.date)
            
            pickExitDate.minimumDate = pickEntryDate.date
        } else {
            exitDateTextField.text = formatter.string(from: pickExitDate.date)

            pickEntryDate.maximumDate = pickExitDate.date
        }
    }
    
    fileprivate func setupTextField() {
        entryDateTextField.inputView = pickEntryDate
        pickEntryDate.datePickerMode = .date
        pickEntryDate.locale = Locale(identifier: locale!)
        pickEntryDate.maximumDate = Date()
        
        exitDateTextField.inputView = pickExitDate
        pickEntryDate.datePickerMode = .date
        pickEntryDate.locale = Locale(identifier: locale!)
        pickEntryDate.maximumDate = Date()
        
        pickEntryDate.addTarget(self, action: #selector(onEditPickEntryDate), for: .valueChanged)
        pickExitDate.addTarget(self, action: #selector(onEditPickExitDate), for: .valueChanged)
        companyNameTextField.addTarget(self, action: #selector(onEditingCompanyName), for: .valueChanged)
        positionTextField.addTarget(self, action: #selector(onEditingPosition), for: .editingChanged)
        entryDateTextField.addTarget(self, action: #selector(onEditingEntryDate), for: .editingChanged)
        exitDateTextField.addTarget(self, action: #selector(onEditingExitDate), for: .editingChanged)
    }
    
    fileprivate func setupView() {
        errorCompanyLabel.isHidden = true
        positionErrorLabel.isHidden = true
        entryDateErrorLabel.isHidden = true
        exitDateTextLabel.isHidden = true
        submitButton.layer.cornerRadius = 10
    }
    
    fileprivate func sendData() {
        guard let viewModel = viewModel else {
            return
        }
        
        let isValidCompany = viewModel.validateCompanyName(company: companyNameTextField.text)
        let isValidPosition = viewModel.validatePosition(position: positionTextField.text)
        let isValidEntryDate = viewModel.validateEntryDate(date: entryDateTextField.text)
        let isValidExitDate = viewModel.validateExitDate(date: exitDateTextField.text)
        
        if isValidCompany, isValidPosition, isValidExitDate, isValidEntryDate {
            self.loadingView.startAnimating()
            
            var postParams: [String: Any] = [:]
            postParams["position"] = positionTextField.text
            postParams["company_name"] = companyNameTextField.text
            postParams["starting_from"] = entryDateTextField.text
            postParams["ending_in"] = exitDateTextField.text
            
            viewModel.postUpdateProfileCareer(postParams: postParams)
        }
    }
}
