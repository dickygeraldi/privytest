//
//  RegistrationViewController.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 16/02/21.
//

import UIKit
import RxSwift
import MapKit
import Toast_Swift

class RegistrationViewController: BaseNetworkViewController<AuthViewModel> {

    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var phoneErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var countryErrorLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var backToLoginButton: UILabel!
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavbar()
        setupTextField()
        setupView()
        
        locManager.requestWhenInUseAuthorization()
    }
    
    override func registerObserver() {
        viewModel?.error.subscribe(onNext: { (message) in
            self.loadingView.stopAnimating()
            
            self.view.makeToast(message)
        }).disposed(by: disposeBag)
        
        viewModel?.errorPhoneNumber.subscribe(onNext: { [weak self] errorMessage in
            guard let vc = self else {
                return
            }
            
            vc.phoneErrorLabel.isHidden = errorMessage.ifNil("").isEmpty
            vc.phoneErrorLabel.text = errorMessage.ifNil("")
        }).disposed(by: disposeBag)
        
        viewModel?.errorPassword.subscribe(onNext: { [weak self] errorMessage in
            guard let vc = self else {
                return
            }
            
            vc.passwordErrorLabel.isHidden = errorMessage.ifNil("").isEmpty
            vc.passwordErrorLabel.text = errorMessage.ifNil("")
        }).disposed(by: disposeBag)
        
        viewModel?.errorCountry.subscribe(onNext: { [weak self] errorMessage in
            guard let vc = self else {
                return
            }
            
            vc.countryErrorLabel.isHidden = errorMessage.ifNil("").isEmpty
            vc.countryErrorLabel.text = errorMessage.ifNil("")
        }).disposed(by: disposeBag)
        
        viewModel?.registrationData.subscribe(onNext: { [weak self] data in
            guard let vc = self, let data = data else {
                return
            }
            
            PreferenceService.saveDeviceKey(data.userDevice?.deviceToken ?? "")
            vc.loadingView.stopAnimating()
            
            if data.userStatus == "pending" {
                let storyboard = UIStoryboard(name: "OtpViewController", bundle: nil)
                let controller = storyboard.instantiateViewController(identifier: "OtpViewController") as? OtpViewController
                controller?.phoneNumber = data.phone
                vc.navigationController?.pushViewController(controller!, animated: true)
            } else {
                vc.view.makeToast("User sudah terdaftar. Silahkan login.")
                vc.navigationController?.popToRootViewController(animated: true)
            }
        }).disposed(by: disposeBag)
    }
    
    @objc func back(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated:true)
    }
    
    @objc func onEditingPhoneNo() {
        _ = viewModel?.validatePhone(phone: phoneTextField.text)
    }
    
    @objc func onEditingPassword() {
        _ = viewModel?.validatePassword(password: passwordTextField.text)
    }
    
    @objc func onEditingCountry() {
        _ = viewModel?.validatePassword(password: countryTextField.text)
    }
    
    @IBAction func sendRegistration(_ sender: Any) {
        sendingData()
    }
}

extension RegistrationViewController {
    fileprivate func setupNavbar() {
        let btnLeftMenu: UIButton = UIButton()
        btnLeftMenu.setImage(UIImage(named: "icon_back"), for: UIControl.State())
        btnLeftMenu.addTarget(self, action: #selector(back(sender:)), for: UIControl.Event.touchUpInside)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    fileprivate func setupView() {
        phoneTextField.setLeftPaddingPoints(10)
        passwordTextField.setLeftPaddingPoints(10)
        countryTextField.setLeftPaddingPoints(10)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(back))
        backToLoginButton.addGestureRecognizer(tap)
        submitButton.layer.cornerRadius = 10
        phoneErrorLabel.isHidden = true
        passwordErrorLabel.isHidden = true
        countryErrorLabel.isHidden = true
    }
    
    fileprivate func getLonLat() -> String {
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            guard let currentLocation = locManager.location else {
                return ""
            }
            
            return "\(currentLocation.coordinate.latitude),\(currentLocation.coordinate.longitude)"
        } else {
            return "123"
        }
    }
    
    fileprivate func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    fileprivate func sendingData() {
        guard let viewModel = viewModel else {
            return
        }
        
        let isValidPhone = viewModel.validatePhone(phone: phoneTextField.text)
        let isValidPass = viewModel.validatePassword(password: passwordTextField.text)
        let isValidCountry = viewModel.validateCountry(country: countryTextField.text)
        
        if isValidPass, isValidPhone, isValidCountry {
            self.loadingView.startAnimating()

            var postParams: [String: Any] = [:]
            postParams["phone"] = phoneTextField.text?.replacingOccurrences(of: "0", with: "62")
            postParams["password"] = passwordTextField.text
            postParams["country"] = countryTextField.text
            postParams["latlong"] = getLonLat()
            postParams["device_token"] = randomString(length: 10)
            postParams["device_type"] = 0

            viewModel.registrationRequest(postParams: postParams)
        }
        let storyboard = UIStoryboard(name: "OtpViewController", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "OtpViewController") as? OtpViewController
        controller?.phoneNumber = phoneTextField.text.ifNil("")
        self.navigationController?.pushViewController(controller!, animated: true)
    }
}

extension RegistrationViewController: UITextFieldDelegate {
    
    func setupTextField() {
        passwordTextField.addTarget(self, action: #selector(onEditingPassword), for: .editingChanged)
        phoneTextField.addTarget(self, action: #selector(onEditingPhoneNo), for: .editingChanged)
        countryTextField.addTarget(self, action: #selector(onEditingCountry), for: .editingChanged)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTextField {
            let characterSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: characterSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        } else {
            return string.count > 0
        }
    }
}
