//
//  GenericViewController.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 15/02/21.
//

import RxSwift
import Foundation
import Toast_Swift

class GenericViewController<D> : UIViewController, ViewModelBased where D: BaseViewModel {
    public var disposeBag = DisposeBag()
    public var viewModel: D?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        setupViewModel()
        self.registerObserver()
    }
    
    private func setupViewModel() {
        if viewModel == nil {
            viewModel = D()
        }
    }
    
    public func registerObserver() {
        viewModel?.message.subscribe(onNext: { [weak self] message in
            if let messages = message, self?.viewIfLoaded?.window != nil {
                self?.view.makeToast(messages)
            }
        }).disposed(by: disposeBag)
    }
}

