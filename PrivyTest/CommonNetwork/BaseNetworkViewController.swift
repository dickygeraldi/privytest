//
//  BaseNetworkViewController.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 15/02/21.
//

import UIKit

class BaseNetworkViewController<D: BaseViewModel>: BaseViewController<D> {
    public func hideKeyboard() {
        self.view.endEditing(true)
    }
}

