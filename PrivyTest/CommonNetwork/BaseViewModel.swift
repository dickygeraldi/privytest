//
//  BaseViewModel.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 15/02/21.
//

import RxCocoa

class BaseViewModel: NSObject {
    public var isLoading = BehaviorRelay<Bool>(value: false)
    public var message = BehaviorRelay<String?>(value: nil)
    
    public required override init() {
        super.init()
    }
}
