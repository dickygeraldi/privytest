//
//  ViewModelBased.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 15/02/21.
//

import UIKit

public protocol ViewModelBased: class {
    associatedtype ViewModel
    var viewModel: ViewModel? { get set }
}
