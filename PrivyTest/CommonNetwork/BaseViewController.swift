//
//  BaseViewController.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 15/02/21.
//

import UIKit
import AlamofireImage

class BaseViewController<D: BaseViewModel>: GenericViewController<D> {
    
    public let loadingView = UIActivityIndicatorView(style: .whiteLarge)
    let blockBackgroundView = UIView.init(frame: .zero)
    
    public var remoteConfigApplied = false

    override func registerObserver() {
        super.registerObserver()
        
        viewModel?.isLoading.subscribe(onNext: { [weak self] (isLoading) in
            if (isLoading) {
                self?.showLoading()
            } else {
                self?.hideLoading()
            }
        }).disposed(by: disposeBag)
    }
    
    func showLoading() {
        loadingView.startAnimating()
    }
    
    public func hideLoading() {
        loadingView.stopAnimating()
    }
    
    fileprivate func setupLoadingView() {
        guard let view = view else {
            return
        }
        
        loadingView.stopAnimating()
        loadingView.color = UIColor.blue
        loadingView.hidesWhenStopped = true
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        
        blockBackgroundView.frame = view.bounds
        blockBackgroundView.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.38)
        view.addSubview(blockBackgroundView)
        
        blockBackgroundView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        blockBackgroundView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        blockBackgroundView.isHidden = true
        
        view.addSubview(loadingView)
        
        loadingView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loadingView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}

