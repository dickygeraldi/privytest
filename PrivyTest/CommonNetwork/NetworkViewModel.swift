//
//  NetworkViewModel.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 15/02/21.
//

import RxCocoa

class NetworkViewModel: BaseViewModel {
    
    private var responseProceeded: Bool = false
    
    public var isTimeout = BehaviorRelay<Bool>(value: false)
    public var timeoutResponse = BehaviorRelay<NetworkResponse?>(value: nil)
    
    public var delegate: NetworkViewModelDelegate?
    
    public func process(response: BehaviorRelay<NetworkResponse?>?, onSuccess: ((NetworkResponse) -> Void)?) {
        responseProceeded = true
        
        guard let response = response, let value = response.value else {
            return
        }
        
        if value.isSuccess() {
            onSuccess?(value)
        } else if value.isTimeoutError() {
            isTimeout.accept(true)
            timeoutResponse = response
        } else {
            self.message.accept(value.errorMessage)
        }
    }
    
    public var gotResponseStatus: Bool {
        return responseProceeded
    }
}

public protocol NetworkViewModelDelegate {
    func onRetry()
}
