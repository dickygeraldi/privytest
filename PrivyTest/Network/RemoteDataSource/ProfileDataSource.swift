//
//  ProfileDataSource.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 18/02/21.
//

import RxCocoa

class ProfileDataSource {
    
    static func getProfile(networkResponse: BehaviorRelay<NetworkResponse?>) {
        BaseApi.get(with: ListUrls.GET_PROFILE) { (response) in
            networkResponse.accept(response)
        }
    }
    
    static func updateProfile(params: [String: Any], networkResponse: BehaviorRelay<NetworkResponse?>) {
        BaseApi.post(with: ListUrls.UPDATE_PROFILE, postParams: params) { (response) in
            networkResponse.accept(response)
        }
    }
    
    static func updateProfileEdu(params: [String: Any], networkResponse: BehaviorRelay<NetworkResponse?>) {
        BaseApi.post(with: ListUrls.UPDATE_PROFILE_EDU, postParams: params) { (response) in
            networkResponse.accept(response)
        }
    }
    
    static func updateProfileCareer(params: [String: Any], networkResponse: BehaviorRelay<NetworkResponse?>) {
        BaseApi.post(with: ListUrls.UPDATE_PROFILE_CAREER, postParams: params) { (response) in
            networkResponse.accept(response)
        }
    }
    
    static func uploadImage(image: UIImage, networkResponse: BehaviorRelay<NetworkResponse?>) {
        BaseApi.uploadImage(ListUrls.UPDATE_COVER_IMAGE, image: image) { (response) in
            networkResponse.accept(response)
        }
    }
    
    static func uploadProfileImage(image: UIImage, networkResponse: BehaviorRelay<NetworkResponse?>) {
        BaseApi.uploadImage(ListUrls.UPDATE_PROFILE_PHOTO, image: image) { (response) in
            networkResponse.accept(response)
        }
    }
}
