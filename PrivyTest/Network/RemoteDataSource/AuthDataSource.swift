//
//  AuthDataSource.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 15/02/21.
//

import RxCocoa

class AuthDataSource {
    
    static func loginRequest(params: [String: Any], networkResponse: BehaviorRelay<NetworkResponse?>) {
        BaseApi.post(with: ListUrls.LOGIN, postParams: params) { (response) in
            networkResponse.accept(response)
        }
    }
    
    static func registrationRequest(params: [String: Any], networkResponse: BehaviorRelay<NetworkResponse?>) {
        BaseApi.post(with: ListUrls.REGISTRATION, postParams: params) { (response) in
            networkResponse.accept(response)
        }
    }
    
    static func otpRequest(params: [String: Any], networkResponse: BehaviorRelay<NetworkResponse?>) {
        BaseApi.post(with: ListUrls.OTP_REQ, postParams: params) { (response) in
            networkResponse.accept(response)
        }
    }
    
    static func validateOtp(params: [String: Any], networkResponse: BehaviorRelay<NetworkResponse?>) {
        BaseApi.post(with: ListUrls.VALIDATE_OTP, postParams: params) { (response) in
            networkResponse.accept(response)
        }
    }
}
