//
//  BaseApi.swift
//  PrivyTest
//
//  Created by Dicky Geraldi on 15/02/21.
//

import Foundation
import Alamofire
import SwiftyJSON

class BaseApi {
    
    public static let publicUrl: String = "http://pretest-qa.dcidev.id/api/v1"
    
    public static let alamofire: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        let sessionManager = Alamofire.SessionManager(configuration: configuration)
        return sessionManager
    }()
    
    static func get(with partUrl: String, params: [String: String] = [:], completion: @escaping (NetworkResponse) -> Void) {
        let fullUrl = publicUrl + partUrl
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        print("Request to: \(fullUrl)")
        
        alamofire.request(fullUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: generateHeader(isUploadImage: false)).responseJSON {
            (dataResponse) -> Void in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            let response = dataResponse
            let httpStatusCode = response.response?.statusCode
            switch(response.result) {
            case .success(let JSON):
                completion(generateNetworkResponse(request: response.request, result: .success(response.result.isSuccess), response: JSON, httpStatusCode: httpStatusCode))
                
            case .failure(let error):
                completion(generateNetworkResponse(request: response.request, result: .failure(error), error: error, httpStatusCode: httpStatusCode))
            }
        }
    }
    
    public static func post(with partUrl: String, postParams: [String: Any] = [:], completion: @escaping (NetworkResponse) -> Void) {
        
        let fullUrl = publicUrl + partUrl
        print("Request to: \(fullUrl)")
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        alamofire.request(fullUrl, method: .post, parameters: postParams, encoding: URLEncoding(), headers: generateHeader(isUploadImage: false)).responseJSON { (dataResponse) -> Void in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            let response = dataResponse
            let httpStatusCode = response.response?.statusCode
            
            switch(response.result) {
            case .success(let JSON):
                completion(generateNetworkResponse(request: response.request, result: .success(response.result.isSuccess), response: JSON, httpStatusCode: httpStatusCode))
                
            case .failure(let error):
                completion(generateNetworkResponse(request: response.request, result: .failure(error), error: error, httpStatusCode: httpStatusCode))
            }
        }
    }
    
    public static func uploadImage(_ partUrl: String, compressionQuality: CGFloat = 0.5, image: UIImage, completion: @escaping (NetworkResponse) -> Void) {
        
        let fullUrl = publicUrl + partUrl
        print("Request to: \(fullUrl)")
        
        let imageData = image.jpegData(compressionQuality: compressionQuality)
        if imageData == nil {
            completion(generateNetworkResponse(request: nil, result: .failure(NSError()), error: "Image not found"))
            return
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData!, withName: "image", fileName: "Photos.jpg", mimeType: "image/jpg")
        }, to: fullUrl, headers: generateHeader(isUploadImage: true), encodingCompletion: { encodingResult in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { dataResponse in
                    let response = dataResponse
                    let httpStatusCode = response.response?.statusCode
                    if response.result.isSuccess {
                        completion(generateNetworkResponse(request: response.request, result: .success(true), response: response.result.value, httpStatusCode: httpStatusCode))
                    } else if response.result.isFailure {
                        completion(generateNetworkResponse(request: response.request, result: .failure(response.result.error!), error: response.result.error, httpStatusCode: httpStatusCode))
                    }
                    
                }
            case .failure(let encodingError):
                completion(generateNetworkResponse(request: nil, result: .failure(encodingError), error: encodingError))
            }
        })
    }
    
    static func generateHeader(isUploadImage: Bool) -> [String: String] {
        var header: [String: String] = [:]
        
        if PreferenceService.isLogin() {
            header["Authorization"] = PreferenceService.getAuthtoken()
        }
        
        if isUploadImage {
            header["Content-Type"] = "multipart/form-data"
        } else {
            header["Content-Type"] = "application/x-www-form-urlencoded"
        }
        
        return header
    }
    
    static func generateNetworkResponse(request: URLRequest?, result: Result<Bool>, response: Any? = nil, error: Any? = nil, httpStatusCode: Int? = nil, isEncrypted: Bool = true) -> NetworkResponse {
        
        var networkResponse = NetworkResponse.init()
        
        if let statusCode = httpStatusCode {
            networkResponse = NetworkResponse.init(withStatusCode: statusCode)
        }
        
        if result.isSuccess && response != nil {
            let json = JSON(response!)
            var realJson: [String: Any] = [:]

            if !json["error"].exists() {
                realJson = json["data"].dictionaryObject!
            } else {
                realJson = json["error"].dictionaryObject!
            }
            
            if !realJson.isEmpty {
                networkResponse.response = realJson
            } else {
                networkResponse.response = realJson
            }
        } else if !networkResponse.isServerError() {
            if let urlError = error as? URLError, urlError.code == .cancelled {
                return networkResponse
            }
            if let urlError = error as? URLError,
               urlError.code == .cannotFindHost || urlError.code == .timedOut {
                networkResponse.statusCode = urlError.code.rawValue
            } else {
                networkResponse.statusCode = NetworkResponse.RETRYABLE_ERROR
            }
        }
        
        networkResponse.request = request
        
        print("Final network response \(networkResponse.response.ifNil(""))")
        print("Final network httpStatusCode \(httpStatusCode.ifNil(0))")
        
        return networkResponse
    }
}
