# Privy Test

This is an test for iOS Developer at Privy.id. This program is contain an iOS apps which is simple application about user profile.

## Installation

You can install the project by running the cocoa pods first. 

```bash
pod install --repo-update
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
